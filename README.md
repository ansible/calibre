# Calibre e-book server

> A *Calibre Library* is a folder containing, at a minimum, a `metadata.db`
> database file. This database is created and managed by the Calibre software 
> itself and represents a single collection of content. You can use the 
> Calibre GUI or the `calibredb` command-line utility to make modifications to 
> this file, but you should never do so by manually editing it yourself.
> https://github.com/shiftctrlspace/ansible-role-calibre


## Maintain Calibre user database (DEPRECATED, handled by Calibre Web)

Create or edit users using the CLI menu, like this:

```
sudo /usr/local/bin/calibre-server --userdb /etc/apache2/calibre-users.sqlite --manage-users
```

Just follow the prompts to create user accounts.

To set their read/write permissions or per-library permissions, create the user first,
then select "edit an existing user".

> NOTE: This is for all practical purposes **no longer important**, as the Calibre-Web
> library has replaced Calibre itself as the de-facto user-facing library service.


## Refs

+ https://calibre-ebook.com
+ https://github.com/shiftctrlspace/ansible-role-calibre
+ https://github.com/shiftctrlspace/library
+ https://grantwinney.com/calibre-is-awesome-calibre-server-is-not
+ https://github.com/baztian/ansible-calibre
